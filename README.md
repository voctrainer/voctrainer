VocTrainer is meant to be a light-weight cross-platform vocabulary trainer, useful on the road on your mobile or for background/passive training while doing other tasks (local or remote). It works with Kvtml vocabularies, which can be created, for example, using Parley.

You can read the full announcement on my blog.
