#ifndef KVTMLPARSER_H
#define KVTMLPARSER_H

#include <QtCore>

class Entry
{
public:
    static void setLang1(QString lang);
    static void setLang2(QString lang);

    Entry(QString lang1word, QString lang2word) : lang1word(lang1word), lang2word(lang2word) {}

    QString getFirstWord(bool reversed = false);
    QString getSecondWord(bool reversed = false);

    static QString lang1;
    static QString lang2;
private:
    QString lang1word;
    QString lang2word;
};

class KvtmlParser
{
public:
    KvtmlParser();

    QList<Entry*> parse(QString filename);
private:
    QString readLangDef(QXmlStreamReader& xml);
    const QList<Entry*> readLesson(QXmlStreamReader& xml, QHash<int, Entry*> entries);
    Entry* readEntry(QXmlStreamReader& xml);
    QString readTranslation(QXmlStreamReader& xml);
    QString filename;
};

#endif // KVTMLPARSER_H
