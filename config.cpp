#include "config.h"

#include "profile.h"

#include <QSettings>
#include <QtDebug>
#include <QDir>

Config* config;

Config::Config()
{
    readStyles();
        read();
        qDebug() << "font size = " << fontSize;
}

Config::~Config()
{
        write();
}

QString Config::getCurrentStyleFullscreen()
{
    return styles[profiles[activeProfile]->styleFullscreen];
}

QString Config::getCurrentStyleNormal()
{
    return styles[profiles[activeProfile]->styleNormal];
}

void Config::read()
{
        qDebug() << "Config::read()";
        QSettings settings;

        windowGeom = settings.value("general/windowGeometry").toByteArray();

        vocFilename = settings.value("general/vocFilename", "").toString();
        timeShowQuestion = settings.value("general/timeShowQuestion", 5000).toInt();
        timeShowSolution = settings.value("general/timeShowSolution", 2000).toInt();
        autoPilot = settings.value("general/autoPilot", false).toBool();
        fontSize = settings.value("general/fontSize", 20).toInt();
        popup = settings.value("general/popup", false).toBool();
        repeat = settings.value("general/repeat", false).toBool();
        activeProfile = settings.value("general/activeProfile", "Default profile").toString();

        Profile* defaultProfile = new Profile("Default profile");
        defaultProfile->styleNormal = "DefaultNormal";
        defaultProfile->styleFullscreen = "DefaultFullscreen";
        profiles[defaultProfile->name] = defaultProfile;
}

void Config::write()
{
        qDebug() << "Config::write()";

        QSettings settings;
        settings.setValue("general/vocFilename", vocFilename);
        settings.setValue("general/timeShowQuestion", timeShowQuestion);
        settings.setValue("general/timeShowSolution", timeShowSolution);
        settings.setValue("general/autoPilot", autoPilot);
        settings.setValue("general/fontSize", fontSize);
        settings.setValue("general/popup", popup);
        settings.setValue("general/repeat", repeat);
}

void Config::readStyles()
{
    styles["DefaultNormal"] = "";
    styles["DefaultFullscreen"] = "background-color: rgb(0, 0, 0); color: rgb(255, 255, 255);";
}
