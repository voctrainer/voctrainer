# -------------------------------------------------
# Project created by QtCreator 2010-01-14T19:26:59
# -------------------------------------------------
VERSION = 0.1-beta
DEFINES += APP_VERSION=$$VERSION
QT += core widgets xml
CONFIG -= console
TARGET = voctrainer
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    kvtmlparser.cpp \
    config.cpp \
    qautofontsizelabel.cpp \
    profile.cpp \
    aboutdialog.cpp
HEADERS += mainwindow.h \
    kvtmlparser.h \
    config.h \
    qautofontsizelabel.h \
    profile.h \
    aboutdialog.h
FORMS += mainwindow.ui \
    settings.ui \
    profiles.ui \
    aboutdialog.ui
OTHER_FILES += 
win32:LIBS += -luser32
RESOURCES += voctrainer.qrc
RC_FILE = voctrainer.rc
