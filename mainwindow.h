#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "aboutdialog.h"

#include <QMainWindow>
#include <QAction>

#include "kvtmlparser.h"

class QShortcut;
class QFileDialog;


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void entryChanged();

public slots:
    void showOpenFileDialog();
    void showHelp();
    void showAboutDialog();
    void loadVoc(QString filename);
    void switchEntry();
    void showSolution();
    void showSolutionOrNext();
    void toggleAutoPilot(bool toggle);
    void setReverseDirection(bool toggle);
    void setRepeat(bool enable);
    void setFullscreen(bool enable);
    void exitFullscreen();

protected:
    void changeEvent(QEvent *e);
    void mousePressEvent ( QMouseEvent * event );
    void closeEvent(QCloseEvent* event);

private:
    void initGUI();
    bool isShowingSolution();

    Ui::MainWindow *ui;
    QFileDialog* openFileDialog;
    AboutDialog* aboutDialog;
    QList<Entry*> words;
    int currentEntryIndex;
    QTimer showTranslationTimer, showNextQuestionTimer;
    uint timeShowQuestion, timeShowSolution;
    bool direction;
    Entry* currentWord;
    QAction* actionSwitchEntry;
    QAction* actionShowSolution;
    QAction* actionExitFullscreen;
    QAction* actionToggleFullscreen;

    QMenu* contextMenu;
    QAction* actionOpen;
    QAction* actionAbout;
    QAction* actionHelp;
};

#endif // MAINWINDOW_H
