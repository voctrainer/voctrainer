#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QByteArray>
#include <QtCore/QList>
#include <QtCore/QHash>

class Profile;

class Config
{
public:
    Config();
    virtual ~Config();

    QString getCurrentStyleNormal();
    QString getCurrentStyleFullscreen();

private:
    void read();
    void write();

    void readStyles();

public: // members
    QByteArray windowGeom;

    QString vocFilename;
    int timeShowQuestion;
    int timeShowSolution;
    bool autoPilot;
    int fontSize;
    bool autoFontSize;
    bool popup;
    bool repeat;
    QString activeProfile;

    QHash<QString, Profile*> profiles;
    QHash<QString, QString> styles;
};

extern Config* config;

#endif // CONFIG_H
