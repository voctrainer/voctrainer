#ifndef PROFILE_H
#define PROFILE_H

#include <QtCore/QString>

class Profile
{
public:
    Profile(QString name);

    QString name;
    QString styleNormal;
    QString styleFullscreen;

};

#endif // PROFILE_H
