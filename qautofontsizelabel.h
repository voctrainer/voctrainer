#ifndef QAUTOFONTSIZELABEL_H
#define QAUTOFONTSIZELABEL_H

#include <QLabel>

class QAutoFontSizeLabel : public QLabel
{
    Q_OBJECT
public:

    QAutoFontSizeLabel ( QWidget * parent = 0, Qt::WindowFlags f = 0 );
    QAutoFontSizeLabel ( const QString & text, QWidget * parent = 0, Qt::WindowFlags f = 0 );

    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *);

    bool autoFontSize;
};

#endif // QAUTOFONTSIZELABEL_H
