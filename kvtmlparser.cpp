#include "kvtmlparser.h"

#include <QtDebug>
#include <QtXml>

/////////////////////////////////////////////////////////////////
//class Entry
/////////////////////////////////////////////////////////////////

//static defs
QString Entry::lang1 = QString();
QString Entry::lang2 = QString();

void Entry::setLang1(QString lang)
{
    Entry::lang1 = lang;
}

void Entry::setLang2(QString lang)
{
    Entry::lang2 = lang;
}

/////////////////////////////////////////////////////////////////

QString Entry::getFirstWord(bool reversed) {
    return reversed ? lang2word : lang1word;
}

QString Entry::getSecondWord(bool reversed) {
    return reversed ? lang1word : lang2word;
}

/////////////////////////////////////////////////////////////////
//class KvtmlParser
/////////////////////////////////////////////////////////////////
KvtmlParser::KvtmlParser()
{
}

QString KvtmlParser::readLangDef(QXmlStreamReader &xml)
{
    QString lang;
    xml.readNext();
    while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
            xml.name() == "identifier")) {
        if (xml.tokenType() == QXmlStreamReader::StartElement) {
            if (xml.name() == "name") {
                lang = xml.readElementText();
            }
        }
        xml.readNext();
    }
    return lang;
}

QString KvtmlParser::readTranslation(QXmlStreamReader& xml)
{
    QString translation;
    xml.readNext();
    while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
            xml.name() == "translation")) {
        if (xml.tokenType() == QXmlStreamReader::StartElement && xml.name() == "text") {
            translation = xml.readElementText();
        }
        xml.readNext();
    }
    return translation;
}

Entry* KvtmlParser::readEntry(QXmlStreamReader& xml)
{
    QString lang1Text, lang2Text;
    xml.readNext();
    while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
            xml.name() == "entry")) {
        if(xml.tokenType() == QXmlStreamReader::StartElement) {
            if(xml.name() == "translation") {
                if (xml.attributes().value("id").toString().toInt() == 0) {
                    lang1Text = readTranslation(xml);
                } else {
                    lang2Text = readTranslation(xml);
                }
            }
        }
        xml.readNext();
    }
    if (!lang1Text.isEmpty() && !lang2Text.isEmpty()) {
        qDebug() << lang1Text << lang2Text;
        return new Entry(lang1Text, lang2Text);
    } else return 0;
}

const QList<Entry*> KvtmlParser::readLesson(QXmlStreamReader& xml, QHash<int, Entry*> entries)
{
    xml.readNext();
    bool active = false;
    QString name;
    QList<Entry*> lessonEntries;
    while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
            xml.name() == "container")) {
        if (xml.tokenType() == QXmlStreamReader::StartElement) {
            if (xml.name() == "name") {
                name = xml.readElementText();
                qDebug() << "reading lesson" << name;
            }
            if (xml.name() == "inpractice") {
                active = xml.readElementText() == "true";
                qDebug() << name << active;
            }
            if ((xml.name() == "entry") && active) {
                int id = xml.attributes().value("id").toString().toInt();
                if (entries.contains(id)) {
                    qDebug() << "adding entry" << id;
                    lessonEntries.append(entries[id]);
                } else {
                    qDebug() << "skipping invalid entry" << id;
                }
            }
            if (xml.name() == "container") {
                lessonEntries.append(readLesson(xml, entries));
            }
        }
        xml.readNext();
    }
    return lessonEntries;
}

QList<Entry*> KvtmlParser::parse(QString filename) {
    QHash<int, Entry*> entries;
    QList<Entry*> activeEntries;
    QList<QString> doubleCheck;
    QList<Entry*> doubles;

    QFile* file = new QFile(filename);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "could not open file" << filename;
        return activeEntries;
    }
    QXmlStreamReader xml(file);

    while(!xml.atEnd() && !xml.hasError()) {
        QXmlStreamReader::TokenType token = xml.readNext();
        qDebug() << xml.name();
        if(token == QXmlStreamReader::StartElement) {
            if(xml.name() == "identifiers") {
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
                        xml.name() == "identifiers")) {
                    if(xml.name() == "identifier") {
                        int id = xml.attributes().value("id").toString().toInt();
                        QString lang = readLangDef(xml);
                        if (id == 0) {
                            Entry::setLang1(lang);
                        } else {
                            Entry::setLang2(lang);
                        }
                    }
                    xml.readNext();
                }
                continue;
            }
            if(xml.name() == "entries") {
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
                        xml.name() == "entries")) {
                    if(xml.name() == "entry") {
                        int id = xml.attributes().value("id").toString().toInt();
                        Entry* entry = readEntry(xml);
                        if (entry != 0) {
                            entries[id] = entry;
                            if(!doubleCheck.contains(entry->getFirstWord())) {
                                doubleCheck.append(entry->getFirstWord());
                            } else {
                                qDebug() << "double detected" << entry->getFirstWord();
                                doubles.append(entry);
                            }
                        }
                    }
                    xml.readNext();
                }
                continue;
            }
            if(xml.name() == "lessons") {
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
                        xml.name() == "lessons")) {
                    if(xml.name() == "container") {
                        activeEntries.append(readLesson(xml, entries));
                    }
                    xml.readNext();
                }
                continue;
            }
        }
    }
    xml.clear();
    foreach (Entry* entry, activeEntries) {
        qDebug() << "active:" << entry->getFirstWord() << entry->getSecondWord();
    }

    foreach (Entry* entry, doubles) {
        qDebug() << "DOUBLE:" << entry->getFirstWord() << "=" << entry->getSecondWord();
    }

    qDebug() << "active entries = " << activeEntries.count() << "of" << entries.count();
    qDebug() << doubles.count() << "doubles";
    qDebug() << "lang :" << Entry::lang1 << " -> " << Entry::lang2;
    return activeEntries;
}
