#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "kvtmlparser.h"
#include "config.h"
#include "profile.h"

#include <QFileDialog>
#include <QtGui>
#include <QtGui/QDesktopServices>
#include <QtCore/QUrl>
#include <QtCore/QCoreApplication>
#include <QMenu>

#if defined( Q_OS_WIN )
#include <Windows.h>
#endif

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    openFileDialog(0),
    aboutDialog(0),
    currentEntryIndex(-1),
    direction(false),
    currentWord(0)
{
    config = new Config();
    ui->setupUi(this);
    initGUI();
    showTranslationTimer.setSingleShot(true);
    showNextQuestionTimer.setSingleShot(true);
    timeShowQuestion = config->timeShowQuestion;
    timeShowSolution = config->timeShowSolution;
    if (QCoreApplication::arguments().size() > 1) {
        config->vocFilename = QCoreApplication::arguments().at(1);
    }
    if (config->vocFilename.isEmpty() || !QFile::exists(config->vocFilename)) {
        showOpenFileDialog();
    } else {
        loadVoc(config->vocFilename);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    delete config;
}

void MainWindow::initGUI()
{
    connect(&showTranslationTimer, SIGNAL(timeout()), this, SLOT(showSolution()));
    connect(&showNextQuestionTimer, SIGNAL(timeout()), this, SLOT(switchEntry()));
    connect(ui->autoPilotButton, SIGNAL(toggled(bool)), this, SLOT(toggleAutoPilot(bool)));
    connect(ui->switchDirectionButton, SIGNAL(toggled(bool)), this, SLOT(setReverseDirection(bool)));
    connect(ui->repeatButton, SIGNAL(toggled(bool)), this, SLOT(setRepeat(bool)));
    ui->autoPilotButton->setChecked(config->autoPilot);
    ui->repeatButton->setChecked(config->repeat);

    actionSwitchEntry = new QAction(tr("Switch to next entry"), this);
    actionSwitchEntry->setShortcut(QKeySequence(tr("right")));
    connect(actionSwitchEntry, SIGNAL(triggered()), this, SLOT(switchEntry()));
    addAction(actionSwitchEntry);

    actionShowSolution = new QAction(tr("Show solution"), this);
    actionShowSolution->setShortcut(QKeySequence(tr("down")));
    connect(actionShowSolution, SIGNAL(triggered()), this, SLOT(showSolutionOrNext()));
    addAction(actionShowSolution);

    actionExitFullscreen = new QAction(tr("Exit fullscreen"), this);
    actionExitFullscreen->setShortcut(QKeySequence(tr("esc")));
    connect(actionExitFullscreen, SIGNAL(triggered()), this, SLOT(exitFullscreen()));
    addAction(actionExitFullscreen);

    actionToggleFullscreen = new QAction("Toggle fullscreen", this);
    actionToggleFullscreen->setShortcut(QKeySequence(tr("alt+f")));
    actionToggleFullscreen->setCheckable(true);
    connect(actionToggleFullscreen, SIGNAL(toggled(bool)), this, SLOT(setFullscreen(bool)));
    connect(actionToggleFullscreen, SIGNAL(toggled(bool)), ui->fullscreenButton, SLOT(setChecked(bool)));
    connect(ui->fullscreenButton,SIGNAL(toggled(bool)), actionToggleFullscreen, SLOT(setChecked(bool)));
    addAction(actionToggleFullscreen);

    restoreGeometry(config->windowGeom);
    QFont font;
    font.setPointSize(config->fontSize);
    ui->lang1Label->setFont(font);
    ui->lang2Label->setFont(font);

    actionAbout = new QAction(QIcon::fromTheme("help-about"),"About", this);
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(showAboutDialog()));

    actionHelp = new QAction(QIcon::fromTheme("help-contents"),"Help", this);
    connect(actionHelp, SIGNAL(triggered()), this, SLOT(showHelp()));

    actionOpen = new QAction(QIcon::fromTheme("file-open"), "Open voc", this);
    connect(actionOpen, SIGNAL(triggered()), this, SLOT(showOpenFileDialog()));

    contextMenu = new QMenu(this);
    contextMenu->addAction(actionOpen);
    contextMenu->addSeparator();
    contextMenu->addAction(actionHelp);
    contextMenu->addAction(actionAbout);
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::mousePressEvent ( QMouseEvent * event ) {
    qDebug() << "mousePressEvent";
    switch(event->button()) {
    case Qt::MidButton:
        qDebug() << "minimize";
        this->setWindowState(windowState() | Qt::WindowMinimized);
        break;
    case Qt::LeftButton:
        if (isShowingSolution()) {
            switchEntry();
        } else {
            showSolution();
        }
        break;
    case Qt::RightButton:
        contextMenu->exec(event->globalPos());
        break;
    }
}


void MainWindow::closeEvent(QCloseEvent* event)
{
    QSettings settings;
    settings.setValue("general/windowGeometry", saveGeometry());
    QWidget::closeEvent(event);
}

void MainWindow::showOpenFileDialog()
{
    if (openFileDialog == 0) {
        openFileDialog = new QFileDialog(this, "Select vocabulary file", QDir::homePath(),
                                         "Vocabulary files (*.kvtml *.xml)");
        openFileDialog->setFileMode(QFileDialog::ExistingFile);
        connect(openFileDialog, SIGNAL(fileSelected(QString)), this, SLOT(loadVoc(QString)));
    }
    openFileDialog->show();
}

void MainWindow::showHelp()
{
    QDesktopServices::openUrl(QUrl("http://mattiesworld.gotdns.org/weblog/coding-excursions/voctrainer"));
}

void MainWindow::showAboutDialog()
{
    if (aboutDialog == 0) {
        aboutDialog = new AboutDialog(this);
    }
    aboutDialog->show();
}

void MainWindow::toggleAutoPilot(bool toggle) {
    config->autoPilot = toggle;
    if (!config->autoPilot) {
        showNextQuestionTimer.stop();
        showTranslationTimer.stop();
    }
    qDebug() << "autopilot = " << config->autoPilot;
}

void MainWindow::setReverseDirection(bool toggle) {
    qDebug() << "MainWindow::setReverseDirection" << toggle;
    this->direction = toggle;
    ui->switchDirectionButton->setToolTip(QString("%1 %3 %2").
                                          arg(Entry::lang1).
                                          arg(Entry::lang2).
                                          arg(direction ? "<-" : "->"));
    switchEntry();
}

void MainWindow::setRepeat(bool enable)
{
    qDebug() << "MainWindow::setRepeat" << enable;
    config->repeat = enable;
}

void MainWindow::setFullscreen(bool enable)
{
    qDebug() << "MainWindow::setFullscreen" << enable;
    if (enable) {
        qDebug() << "Switching style to " << config->getCurrentStyleFullscreen();
        setStyleSheet(config->getCurrentStyleFullscreen());
        showFullScreen();
    } else {
        qDebug() << "Switching style to " << config->getCurrentStyleNormal();
        setStyleSheet(config->getCurrentStyleNormal());
        showNormal();
    }
}

void MainWindow::exitFullscreen()
{
    qDebug() << "MainWindow::exitFullscreen()";
    actionToggleFullscreen->setChecked(false);
}

void MainWindow::showSolution() {
    if (currentWord == 0) return;
    ui->lang2Label->setText(currentWord->getSecondWord(direction));
    if (config->autoPilot) showNextQuestionTimer.start(timeShowSolution);
}

void MainWindow::showSolutionOrNext() {
    if (ui->lang2Label->text().isEmpty()) showSolution();
    else switchEntry();
}

void MainWindow::loadVoc(QString filename) {
    if (filename.isEmpty()) return;
    currentWord = 0;
    words.clear();
    KvtmlParser parser;
    words = parser.parse(filename);
    qsrand(QDateTime::currentDateTime().toTime_t());
    setReverseDirection(ui->switchDirectionButton->isChecked());
}

void MainWindow::switchEntry() {
    qDebug() << "word size" << words.size();
    if (words.size() == 0) {
        if (config->repeat) {
            loadVoc(config->vocFilename);
        } else {
            ui->wordsLeftLabel->setText("no words left");
        }
        return;
    }
    currentEntryIndex = (float)qrand() / (float)RAND_MAX * (words.size() - 1);
    qDebug() << "currentEntryIndex = " << currentEntryIndex;
    currentWord = words[currentEntryIndex];
    ui->lang1Label->setText(currentWord->getFirstWord(direction));
    ui->lang2Label->setText("");
    words.removeAt(currentEntryIndex);
    ui->wordsLeftLabel->setText(QString("%1 words left").arg(words.size()));
    if (config->autoPilot) {
        showTranslationTimer.start(timeShowQuestion);
        showNextQuestionTimer.stop();
    }
    if (config->popup) {
        qDebug() << "popup";
#if defined( Q_OS_WIN )
        SetWindowPos( winId(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
        SetWindowPos( winId(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
#else
        setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);
        setWindowFlags(windowFlags() ^ Qt::WindowStaysOnTopHint);
        show();
#endif
    }
}

bool MainWindow::isShowingSolution() {
    return ui->lang2Label->text() != "";
}
