#include <QApplication>
#include "mainwindow.h"

#include <QtCore>

#define QUOTE_(x) #x
#define QUOTE(x) QUOTE_(x)

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("voctrainer");
    QCoreApplication::setApplicationVersion(QUOTE(APP_VERSION));

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
