#include "qautofontsizelabel.h"

#include <QStyleOption>
#include <QString>
#include <QPainter>
#include <QtDebug>

QAutoFontSizeLabel::QAutoFontSizeLabel ( QWidget * parent, Qt::WindowFlags f)
    : QLabel(parent, f), autoFontSize(true)
{
}

QAutoFontSizeLabel::QAutoFontSizeLabel ( const QString & text, QWidget * parent, Qt::WindowFlags f)
    : QLabel(text, parent, f), autoFontSize(true)
{

}

void QAutoFontSizeLabel::paintEvent(QPaintEvent * e)
{
    if (!autoFontSize) QLabel::paintEvent(e);
    else {
        QStyle *style = QWidget::style();
        QPainter painter(this);
        drawFrame(&painter);
        QRect cr = contentsRect();

        cr.adjust(this->margin(), this->margin(), -this->margin(), -this->margin());
        int align = QStyle::visualAlignment(layoutDirection(), QFlag(this->alignment()));
        QStyleOption opt;
        opt.initFrom(this);
        int flags = align;
        float fontScale;
        QRect textBr = painter.fontMetrics().boundingRect(text());
        float textRatio = (float)textBr.width() / (float)textBr.height();
        float labelRatio = (float)cr.width() / (float)cr.height();
        if (textRatio > labelRatio) {
            fontScale = (float)cr.width() / (float)textBr.width();
        } else {
            fontScale = (float)cr.height() / (float)textBr.height();
        }
        QFont f = painter.font();
        f.setPointSizeF(f.pointSizeF() * fontScale);
        painter.setFont(f);
        style->drawItemText(&painter, cr, flags, opt.palette, isEnabled(), text(), foregroundRole());
    }
}

QSize QAutoFontSizeLabel::sizeHint() const
{
    return QLabel::sizeHint();
}
