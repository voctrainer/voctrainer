#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    ui->versionLabel->setText(QString("v%1").arg(QApplication::applicationVersion()));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
